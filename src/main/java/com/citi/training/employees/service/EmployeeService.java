package com.citi.training.employees.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.employees.dao.EmployeeDao;
import com.citi.training.employees.model.Employee;

@Component
public class EmployeeService {
	/*
	 * Extra layer allows us to communicate up to controller and down to DAO
	 * Instead of controller communicating with DAO directly this is a middle layer
	 */
	
	@Autowired
	private EmployeeDao employeeDao;
	
	public List<Employee> findAll(){
		return employeeDao.findAll();
	}

	public Employee findById(int id) {
		return employeeDao.findById(id);
	}

	public Employee create(Employee employee) {
		return employeeDao.create(employee);
	}

	public void deleteById(int id) {
		employeeDao.deleteById(id);
	}
	
}
