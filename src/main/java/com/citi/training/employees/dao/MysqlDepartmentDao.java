package com.citi.training.employees.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

//import com.citi.training.employees.dao.MysqlDepartmentDao.DepartmentMapper;
import com.citi.training.employees.exceptions.DepartmentNotFoundException;
import com.citi.training.employees.model.Department;

@Component
public class MysqlDepartmentDao implements DepartmentDao {
	
    @Autowired
    JdbcTemplate tpl;

	@Override
	public List<Department> findAll() {
        return tpl.query("SELECT id, name, salary FROM departments",
                new DepartmentMapper());
	}

	@Override
	public Department findById(int id) {
        List<Department> department = tpl.query(
                "SELECT id, name, salary FROM departments WHERE id=?",
                new Object[] {id},
                new DepartmentMapper()
        );
        
        if(department.size() <= 0) {
            throw new DepartmentNotFoundException("Department with id=[" + id +
                                                 "] not found");
        }
        
        return department.get(0);
	}

	@Override
	public Department create(Department department) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

        tpl.update(
           new PreparedStatementCreator() {
               @Override
               public PreparedStatement createPreparedStatement(Connection connection)
                                                                   throws SQLException {

                   PreparedStatement ps =
                           connection.prepareStatement(
                                   "insert into departments (name) values (?)",
                           Statement.RETURN_GENERATED_KEYS);
                   ps.setString(1,  department.getName());
                   return ps;

               }
           },
           keyHolder
         );
        department.setId(keyHolder.getKey().intValue());
        return department;
	}

	@Override
	public void deleteById(int id) {
        findById(id);
        tpl.update("DELETE FROM departments WHERE id=?", id);
	}
	
    private static final class DepartmentMapper implements RowMapper<Department>{

        public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Department(rs.getInt("id"),
                                rs.getString("name"));
        }

    }

}
