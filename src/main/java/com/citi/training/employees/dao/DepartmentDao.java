package com.citi.training.employees.dao;

import java.util.List;

import com.citi.training.employees.model.Department;

public interface DepartmentDao {
	
	List<Department> findAll();

	Department findById(int id);

	Department create(Department department);

    void deleteById(int id);
    
}
