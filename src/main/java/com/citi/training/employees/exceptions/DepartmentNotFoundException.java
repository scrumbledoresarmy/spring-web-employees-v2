package com.citi.training.employees.exceptions;

@SuppressWarnings("serial")
public class DepartmentNotFoundException extends RuntimeException {

	    public DepartmentNotFoundException(String msg) {
	        super(msg);
	    }
}