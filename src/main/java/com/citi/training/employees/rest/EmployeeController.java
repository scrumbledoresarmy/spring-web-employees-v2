package com.citi.training.employees.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.employees.dao.EmployeeDao;
import com.citi.training.employees.model.Employee;
import com.citi.training.employees.service.EmployeeService;


/**
 * REST interface for {@link com.citi.training.employees.model.Employee} domain object.
 * 
 * @author Administrator
 * @see Employee
 * @see <A href="http://google.com">Google</a>
 */
@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);
 
    @Autowired
    private EmployeeService employeeService;

    /**
     * Find all {@link com.citi.training.employees.model.Employee}.
     * @return printed string of all hashmap {@link com.citi.training.employees.model.Employee}
     */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> findAll(){
        LOG.debug("findAll() was called");
        return employeeService.findAll();
    }
    
    /**
     * Find an {@link com.citi.training.employees.model.Employee} by it's integer id.
     * @param id the id of the {@link com.citi.training.employees.model.Employee} to find
     * @return {@link com.citi.training.employees.model.Employee} that was found or HTTP 404.
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Employee findById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        return employeeService.findById(id);
    }

    /**
     * Create an {@link com.citi.training.employees.model.Employee} by giving object.
     * @param employee object constructed with String name and double salary.
     * @return {@link com.citi.training.employees.model.Employee} the object (id, name, salary)
     */
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Employee> create(@RequestBody Employee employee) {
        LOG.debug("create was called, employee: " + employee);
        return new ResponseEntity<Employee>(employeeService.create(employee),
                                            HttpStatus.CREATED);
    }

    /**
     * Delete an {@link com.citi.training.employees.model.Employee} by it's integer id.
     * @param id the id of the {@link com.citi.training.employees.model.Employee} to delete
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        employeeService.deleteById(id);
    }
}
