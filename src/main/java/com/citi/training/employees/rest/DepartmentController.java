package com.citi.training.employees.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.employees.dao.DepartmentDao;
import com.citi.training.employees.model.Department;

@RestController
@RequestMapping("/departments")
public class DepartmentController {
	
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);
	 
    @Autowired
    private DepartmentDao departmentDao;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Department> findAll(){
        LOG.debug("findAll() was called");
        return departmentDao.findAll();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Department findById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        return departmentDao.findById(id);
    }

    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Department> create(@RequestBody Department department) {
        LOG.debug("create was called, department: " + department);
        return new ResponseEntity<Department>(departmentDao.create(department),
                                            HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        departmentDao.deleteById(id);
    }
}
